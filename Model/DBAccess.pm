package Model::DBAccess;
use DBI;
my $DS = "dbi:mysql:training_db:localhost";
my $USER = "root";
my $PASS = "";
my $OPTS = { AutoCommit => 0 };
my $table = "course";

sub new {
  my $class = shift;
  my $args = ref $_[0] ? $_[0] : +{@_};
  bless $args, $class;
}
sub open{
  my $dbh = DBI->connect($DS, $USER, $PASS, $OPTS) || die $!;
  return $dbh;
}

sub records_searched_by_keyword{
  my $dbh = shift->open();
  my $key = shift;
  my $stmt = $dbh->prepare(<<"SQL");
    select * from course
    where
    course_title like ? or 
    topic like ? or 
    category like ? 
    order by course_id
SQL

  $stmt->execute("%$key%","%$key%","%$key%");
  my @all_records = [];
  while (my $ref = $stmt->fetchrow_hashref()){
    push @all_records , $ref;
  }
  my $rows = $stmt->rows;
  $dbh->disconnect();
  return \@all_records, $rows;
  }
sub records_searched_by_category{
  my $dbh = shift->open();
  my $key = shift;
  my $stmt = $dbh->prepare(<<"SQL");
    select * from course
    where
    category like ? 
    order by course_id
SQL

  $stmt->execute("%$key%");
  my @all_records = [];
  while (my $ref = $stmt->fetchrow_hashref()){
    push @all_records , $ref;
  }
  my $rows = $stmt->rows;
  $dbh->disconnect();
  return \@all_records, $rows;
  }



sub all_records{
  my $dbh = shift->open();
  my $stmt = $dbh->prepare(<<"SQL");
    select * from course order by course_id
SQL

  $stmt->execute();
  my @all_records = [];
  while (my $ref = $stmt->fetchrow_hashref()){
    push @all_records , $ref;
  }
  my $rows = $stmt->rows;
  $dbh->disconnect();
  return \@all_records, $rows;
  }

  sub one_record{
    my $dbh = shift->open();
    my $id = shift;
    my $stmt = $dbh->prepare(<<"SQL");
  select * from course where course_id = ?
SQL
    $stmt->execute($id);
    my $ref =  $stmt->fetchrow_hashref();
    $dbh->disconnect();
    return $ref;
  }
  sub insert{
    my $dbh = shift->open();
    my $cgi = shift;
    my $stmt = $dbh->prepare(<<"SQL");
      insert into course values 
      (?, ?, ?, ?, ?, ?, ?)
SQL
    $stmt->bind_param(1, $cgi->param("course_id"));
    $stmt->bind_param(2, $cgi->param("course_title"));
    $stmt->bind_param(3, $cgi->param("topic"));
    $stmt->bind_param(4, $cgi->param("day_length"));
    $stmt->bind_param(5, $cgi->param("price"));
    $stmt->bind_param(6, $cgi->param("level_id"));
    $stmt->bind_param(7, $cgi->param("category"));
    $res = $stmt->execute();
    $dbh->disconnect();
    return $res;
  }

  sub update{
    my $dbh = shift->open();
    my $cgi = shift;
    my $stmt = $dbh->prepare(<<"SQL");
      update course set
      course_title = ?,
      topic = ?,
      day_length = ?,
      price = ?,
      level_id =?,
      category = ?
      where course_id = ?
SQL
    $stmt->bind_param(1, $cgi->param("course_title"));
    $stmt->bind_param(2, $cgi->param("topic"));
    $stmt->bind_param(3, $cgi->param("day_length"));
    $stmt->bind_param(4, $cgi->param("price"));
    $stmt->bind_param(5, $cgi->param("level_id"));
    $stmt->bind_param(6, $cgi->param("category"));
    $stmt->bind_param(7, $cgi->param("course_id"));
    $stmt->execute();
    $dbh->disconnect();
  }
  sub delete{
  my $dbh = shift->open();
    my $id = shift;
    my $stmt = $dbh->prepare(<<"SQL");
      delete from course where course_id = ?
SQL
    $stmt->execute($id);
    $dbh->disconnect();
    #return $ref;
  }




