package Model::Scrubber;
use HTML::Scrubber;

sub scrub{
  my $cgi = shift;
  my $scrubber = HTML::Scrubber->new;
  for $head  (qw/course_id course_title topic day_length price, level_id, category  /){
    $new = $scrubber->scrub($cgi->param("$head"));
    $new = trim($new);
    $cgi->param("$head" => $new);
  }
}
sub trim{
  my $val = shift;
  $val =~ s/^ *(.*?) *$/$1/;
  return $val;
}
1;
