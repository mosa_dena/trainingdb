package Model::Validation;
use FormValidator::Simple;
use Template;
use Data::Dumper;
local $Data::Dumper::Indent = 1;
local $Data::Dumper::Terse  = 1;

sub new{
  my $class = shift;
  my $args = ref $_[0] ? $_[0] : +{@_};
  bless $args, $class;
}
sub error_check{
  $self = shift;

  my $result = FormValidator::Simple->check( $self->{cgi} => [
      course_id => ['NOT_BLANK', ['REGEX', qr/^([a-zA-Z0-9]|-)+$/], ['LENGTH', 1, 20]],
      course_title => ['NOT_BLANK', ['REGEX', qr/^([a-zA-Z0-9]| )+$/], ['LENGTH', 1, 50]],
      topic => [['LENGTH', 0, 100]],
      day_length  => ['NOT_BLANK', 'INT', ['BETWEEN',1, 99 ]],
      price  => ['NOT_BLANK', 'INT', ['BETWEEN',0, 99999 ]],
      level_id  => ['NOT_BLANK', 'INT', ['BETWEEN',1, 5 ]],
      category => ['NOT_BLANK', ['REGEX', qr/^[a-zA-Z0-9]+$/], ['LENGTH', 1, 40]],
    ] );

  if ( $result->has_error ) {
    my $tt = Template->new({ INCLUDE_PATH => 'View/template' });
    $tt->process('error_validation.tmpl', { result => $result }) || print $tt->error();
    return 1;
  }
  return 0;
}
