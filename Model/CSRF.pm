package Model::CSRF;
use Digest::SHA1 qw(sha1_base64);
use Time::HiRes;

sub create_token{
  my $secret_key = &_get_secret_key();
  my $ip = $ENV{'REMOTE_ADDR'};
  my $msec = Time::HiRes::time();
  my $csrf_token = $msec . "-" . sha1_base64($msec . $ip . $secret_key); 
  return $csrf_token;
}

sub validate_token{
  my $cgi = shift;
  my $secret_key = &_get_secret_key();
  my $ip = $ENV{'REMOTE_ADDR'};
  my $req_token = $cgi->param('csrf_token');
  my ($msec, $k) = split(/-/, $req_token);
  my $calc_token = $msec . '-' . sha1_base64($msec . $ip . $secret_key);
  if ($req_token eq $calc_token && $msec > time() - 600) {
    return 1;
  }
  return 0; 
}

sub _get_secret_key {
  my $key_file = "/usr/local/etc/secret-key.txt";
  open(F, "<$key_file") || return -1;
  my $key = <F>;
  chomp($key);
  close(F);
  return $key;
}
1;
