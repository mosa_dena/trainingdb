package Controller;

sub new {
  my $class = shift;
  my $args = ref $_[0] ? $_[0] : +{@_};
  bless $args, $class;
}

sub before(){
  print "Content-Type: text/html;charset=utf8\n\n";
  my $template_header = new HTML::Template( filename => 'View/template/header.tmpl' );
  print $template_header->output();
}
sub build{}

sub after(){
  my $template_footer = new HTML::Template( filename => 'View/template/footer.tmpl' );
  print $template_footer->output();
}
sub output(){
  my $self = shift;
  $self->before();
  $self->build(shift); $self->after();
}

sub template_binder{
  my ($self, $template_body, $course) = @_;
  $template_body->param(course_id => $course->{course_id});
  $template_body->param(course_title => $course->{course_title});
  $template_body->param(topic => $course->{topic});
  $template_body->param(day_length => $course->{day_length});
  $template_body->param(price => $course->{price});
  $template_body->param(level_id => $course->{level_id});
  $template_body->param(category => $course->{category});
}
sub build_error{
  my ($self, $error_message) = @_;
  my $error_template_body = new HTML::Template( filename => 'View/template/error.tmpl' );
  $error_template_body->param(error_message => $error_message);
 print $error_template_body->output();
}
sub validation{
  my($self, $cgi) = @_;
  $val = Model::Validation->new(cgi => $cgi);
  $error_message = $val->error_check;
  return $error_message;
}

1;
