package Controller::CourseAll;
use base qw(Controller);
sub build(){
  my ($self, $cgi, $courses, $rows, $page_title) = @_; 

  ($courses, $rows) = Model::DBAccess->new->all_records unless defined $courses ;
  $page_title = "all courses" unless defined $page_title;
  my $template_body = new HTML::Template( filename => 'View/template/all.tmpl' );
  $template_body->param(records_count => $rows );
  $template_body->param(page_title => $page_title);
  $template_body->param(courses => $courses);
  print $template_body->output();
}
sub dispacher(){
  my ($self, $cgi, $courses, $rows) = @_;
  if($rows == 1){
    $id = $courses->[1]{course_id};
    $cgi->param("course_id" => $id);
    Controller::CourseID->new->build($cgi);
  }else{

    #why?
    #print $self;
    #$self->SUPER::build($cgi, $courses, $rows, "course found");
    &build($self, $cgi, $courses, $rows, "course found");
  }
}
1;
