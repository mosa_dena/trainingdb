package Controller::CourseGotDeleted;
use base qw(Controller);
sub build(){
  my $self = shift;
  my $cgi = shift;
  unless (Model::CSRF::validate_token($cgi)){
    $self->build_error("csrf-token invaild.");
    return;
  }
  my $dba = Model::DBAccess->new;
  my $id = $cgi->param("course_id");
  $dba->delete($id);
  my $template_body = new HTML::Template( filename => 'View/template/got_deleted.tmpl' );
  $template_body->param(course_id => $id);

 print $template_body->output();
}
1;

