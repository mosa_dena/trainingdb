package Controller::CourseIDUpdate;
use base qw(Controller::CourseID);
sub build(){
  my $self = shift;
  my $cgi = shift;
  unless (Model::CSRF::validate_token($cgi)){
    $self->build_error("csrf-token invaild.");
    return;
  }
  my $dba = Model::DBAccess->new();
  return if (my $message = $self->validation($cgi) );
  Model::Scrubber::scrub($cgi);
  $dba->update($cgi);

  $self->SUPER::build($cgi, "course updated");

}
1;

