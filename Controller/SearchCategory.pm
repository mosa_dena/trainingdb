package Controller::SearchCategory;
use base qw(Controller::CourseAll);

sub build(){
  my ($self, $cgi) = @_; 
  my $key = $cgi->param("category");
  if($key eq ""){
    $self->build_error("please input a category");
    return;
  }
  my ($courses, $rows) = Model::DBAccess->new->records_searched_by_category($key);
  $self->dispacher($cgi, $courses, $rows);
}
1
