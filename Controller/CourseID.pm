package Controller::CourseID;
use base qw(Controller);
sub build(){
  my $self = shift;
  my $cgi = shift;
  my $page_title =  shift || "single course";
  my $dba = Model::DBAccess->new;
  
  my $id = $cgi->param("course_id");
  my $course = $dba->one_record($id);
  my $csrf_token = Model::CSRF::create_token();
  my $template_body = new HTML::Template( filename => 'View/template/course_id.tmpl' );

  $self->template_binder($template_body, $course);
  $template_body->param(page_title=> $page_title);
  $template_body->param(csrf_token => $csrf_token);
  
 print $template_body->output();
}
1;

