package Controller::SearchKey;
use base qw(Controller::CourseAll);

sub build(){
  my ($self, $cgi) = @_;
  my $key = $cgi->param("key");
  if($key eq ""){
    $self->build_error("please input a keyword");
    return;
  }
  my ($courses, $rows) = Model::DBAccess->new->records_searched_by_keyword($key);
  $self->dispacher($cgi, $courses, $rows);
 }
1;
